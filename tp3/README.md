# TP3

For this project we will be simulating a connection between networks that could simulate the one inside a real entreprise. For this, it is important to have correctly understood TP2. We will continue to use the same tools as previously.

Furtehrmore, we will continue to imporve our knowledge of informatic services and by the end of the project I will have a working DHCP server, Web server and DNS server that a client must be able to use.

## 1. Topology and addressing table

We will have three different virtual LAN connected to each other:

- First LAN

This LAN is composed of node1.net1.tp3, node2.net1.tp3 to which will I will attribute 10.3.1.11/24 and 10.3.1.12/24 respectively. These machines will be connected between each other through a switch device, that will at the same time be connected to router.net1.tp3.

The router will work as a gateway for packages that leave the net. It must have the following IP address: 10.3.1.254 on the same interface as the thwo nodes.

- Second LAN

On the right wing of our GNS, I will replicate the LAN but with some modifications. Nodes now are called: node1.net2.tp3 and node2.net2.tp3 and will have the following IP addresses: 10.3.2.11/24 and 10.3.2.12/24, respectively. Like on the previous LAN, they will be connected to router2.nat2.tp3 (10.3.2.254/24) and between each other through a switch.

- Third LAN

This LAN is a little bit more complex, it is composed of both routers. I will connect them by giving router1.nat1.tp3 the IP address of 10.3.100.1/30 and router2.nat2.tp3 will have 10.3.100.2/30. Both will be configured on the same interface (in this case enp0s8). At the same time, router1.net1.tp3 will have NAT connection in order to give my virtual enviroment internet access. A dynamic IP address must be given to the interface that supports the LAN connection

Once this is done, I will have to stablish the routers IP as a static route between the LANs. The goal is for nodes on different LANs to be able to ping eachother. On the other hand, nodes will not be able to ping LAN 3.


### Stage 1: IP configuration

On the following lines I will show the commands and the files I modified about every machine:

- **Node1.net1.tp3**

Firstly, a hostname and static IP address will be given:

 *$ sudo vi /etc/hostname/*

    node1Net1

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.1.11
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

    [root@node1Net1 amelendez]# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:68:fb:43 brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.11/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
        valid_lft 592sec preferred_lft 592sec
        inet6 fe80::a00:27ff:fe68:fb43/64 scope link noprefixroute
        valid_lft forever preferred_lft forever

- **Node2.net1.tp3**

 *$ sudo vi /etc/hostname/*

    node2Net1

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.1.12
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

   [amelendez@node2Net1 ~]$ ip a
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
         valid_lft forever preferred_lft forever
   2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      link/ether 08:00:27:23:be:b6 brd ff:ff:ff:ff:ff:ff
      inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
         valid_lft forever preferred_lft forever
      inet6 fe80::a00:27ff:fe23:beb6/64 scope link
         valid_lft forever preferred_lft forever

- **Router1.net1.tp3**

I will have to configure three interfaces here

 *$ sudo vi /etc/hostname/*

    router1

1. enp0s3

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.1.254
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

2. enp0s8

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8*

    NAME=enp0s8
    DEVICE=enp0s8

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.100.1
    NETMASK=255.255.255.252

 *$ nmcli con reload*
 *$ nmcli con up enp0s8*

3. enp0s9

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s9*

    NAME=enp0s9
    DEVICE=enp0s9

    BOOTPROTO=dhcpd
    ONBOOT=yes

 *$ nmcli con reload*
 *$ nmcli con up enp0s9*

    [amelendez@router1 ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:36:09:cf brd ff:ff:ff:ff:ff:ff
        inet 10.3.1.254/24 brd 10.3.1.255 scope global noprefixroute enp0s3
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe36:9cf/64 scope link
        valid_lft forever preferred_lft forever
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:4b:1f:c0 brd ff:ff:ff:ff:ff:ff
        inet 10.3.100.1/30 brd 10.3.100.3 scope global noprefixroute enp0s8
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe4b:1fc0/64 scope link
        valid_lft forever preferred_lft forever
    4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:8a:44:2e brd ff:ff:ff:ff:ff:ff
        inet 192.168.164.133/24 brd 192.168.164.255 scope global dynamic noprefixroute enp0s9
        valid_lft 1620sec preferred_lft 1620sec
        inet6 fe80::a00:27ff:fe8a:442e/64 scope link
        valid_lft forever preferred_lft forever

  - **Node1.nat2.tp3**

 *$ sudo vi /etc/hostname/*

    node1Net2

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.2.11
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

    [amelendez@node1Net2 ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:dc:89:0e brd ff:ff:ff:ff:ff:ff
        inet 10.3.2.11/24 brd 10.3.2.255 scope global noprefixroute enp0s3
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fedc:890e/64 scope link
        valid_lft forever preferred_lft forever

- **Node2.net2.tp3**

*$ sudo vi /etc/hostname/*

    node2Net2

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.2.12
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

    [amelendez@node2Net2 ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:b0:18:83 brd ff:ff:ff:ff:ff:ff
        inet 10.3.2.12/24 brd 10.3.2.255 scope global noprefixroute enp0s3
        valid_lft forever preferred_lft forever
        valid_lft forever preferred_lft forever

- **Router2.net2.tp3**

*$ sudo vi /etc/hostname/*

    router2

1. enp0s3

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.2.254
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

2. enp0s8
   
 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8*

    NAME=enp0s8
    DEVICE=enp0s8

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.3.100.2
    NETMASK=255.255.255.252

 *$ nmcli con reload*
 *$ nmcli con up enp0s8*

    [amelendez@router2 ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:1f:00:17 brd ff:ff:ff:ff:ff:ff
        inet 10.3.2.254/24 brd 10.3.2.255 scope global noprefixroute enp0s3
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe1f:17/64 scope link
        valid_lft forever preferred_lft forever
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:8f:ef:b5 brd ff:ff:ff:ff:ff:ff
        inet 10.3.100.2/30 brd 10.3.100.3 scope global noprefixroute enp0s8
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fe8f:efb5/64 scope link
        valid_lft forever preferred_lft forever

#### Testing

- **LAN 1**

1. node1.net1.tp3

   [amelendez@node1Nat1 ~]$ ping 10.3.1.254
    PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
    64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.042 ms

   [amelendez@node1Nat1 ~]$ ping 10.3.1.12
    PING 10.3.1.254 (10.3.1.12) 56(84) bytes of data.
    64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.059 ms

2. node2.net1.tp3

    [amelendez@node2Nat1 ~]$ ping 10.3.1.254
    PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
    64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=1.02 ms

- **LAN 2**

1. node1.net2.tp3

    [root@node1Net2 amelendez]# ping 10.3.2.12
    PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
    64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=1.64 ms

    [amelendez@node1Net2 ~]$ ping 10.3.2.254
    PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
    64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=0.889 ms

2. node2.net.tp3

    [amelendez@node2Net2 ~]$ ping 10.3.2.254
    PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
    64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=1.55 ms

- **LAN 3**

      [amelendez@router1 ~]$ ping 10.3.100.2
      PING 10.3.100.1 (10.3.100.2) 56(84) bytes of data.
      64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=0.087 ms

      [amelendez@router1 ~]$ ping efrei.fr
      PING efrei.fr (51.255.68.208) 56(84) bytes of data.
      64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=1 ttl=128time=16.3 ms

### Stage 2: Local routes

Firsly I will activate routing on both routers so that they pass on the messages that are not destined to them:

   [amelendez@router1 ~]$ sudo sysctl -w net.ipv4.ip_forward=1 
   net.ipv4.ip_forward = 1

   [amelendez@router1 ~]$ sudo firewall-cmd --add-masquerade
   success

   [amelendez@router1 ~]$ sudo firewall-cmd --add-masquerade --permanent
   success

To be able to ping each other, as I mentionned before it is necessary to cofigure some local routes that will redirect the packages into the wanted network:

- Node1.net1.tp3

 *$ vi /etc/sysconfig/network-scripts/route-enp0s3*

    10.3.2.0/24 via 10.3.1.254 dev enp0s3

    [root@node1Net1 amelendez]# ip r s
    10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
    10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100

What I did here is to add LAN's one router's IP address as gateway in order to packages destined to the second LAN. And I will do the same in each other node of the different LANs

- Node2.net1.tp3

 *$ vi /etc/sysconfig/network-scripts/route-enp0s3*

    10.3.2.0/24 via 10.3.1.254 dev enp0s3

    [amelendez@node2Nat1 ~]$ ip r s
    10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 100
    10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100

- Node1.net2.tp3

 *$ vi /etc/sysconfig/network-scripts/route-enp0s3*

    10.3.1.0/24 via 10.3.2.254 dev enp0s3

    [amelendez@node1Net2 ~]$ ip r s
    10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100
    10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.11 metric 100

- Node2.net2.tp3

 *$ vi /etc/sysconfig/network-scripts/route-enp0s3*

    10.3.1.0/24 via 10.3.2.254 dev enp0s3

    [amelendez@node2Net2 ~]$ ip r s
    10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 102
    10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 102

Now I have to modify the interface that corresponds to the LAN 3. 

- Router1.net1.tp3    

*$ vi /etc/sysconfig/network-scripts/route-enp0s8*

   10.3.2.0/24 via 10.3.100.2 dev enp0s8

   [amelendez@router1 ~]$ ip r s
   default via 192.168.164.2 dev enp0s9 proto dhcp src 192.168.164.133 metric 103
   10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.254 metric 101
   10.3.2.0/24 via 10.3.100.2 dev enp0s8 proto static metric 102
   10.3.100.0/30 dev enp0s8 proto kernel scope link src 10.3.100.1 metric 102
   192.168.164.0/24 dev enp0s9 proto kernel scope link src 192.168.164.133 metric 103

- Router2.net2.tp3

 *$ vi /etc/sysconfig/network-scripts/route-enp0s8*

   10.3.1.0/24 via 10.3.100.1 dev enp0s8

   [amelendez@router2 ~]$ ip r s
   10.3.1.0/24 via 10.3.100.1 dev enp0s8 proto static metric 101
   10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.254 metric 102
   10.3.100.0/30 dev enp0s8 proto kernel scope link src 10.3.100.2 metric 101

Note that I did not add any routes in order to LAN1 or LAN2 to be able to ping LAN3. 

#### Ping testing

Once my routes are stablished I wil now test pings from LAN1 to LAN2 and the other way round so I make sure all commands were succesfull:

- Node1.net1.tp3

   [amelendez@node1Net1 ~]$ ping 10.3.2.101
   PING 10.3.2.101 (10.3.2.101) 56(84) bytes of data.
   64 bytes from 10.3.2.101: icmp_seq=1 ttl=62 time=4.23 ms

   [amelendez@node1Net1 ~]$ ping 10.3.2.102
   PING 10.3.2.102 (10.3.2.102) 56(84) bytes of data.
   64 bytes from 10.3.2.102: icmp_seq=1 ttl=62 time=5.14 ms

- Node2.net1.tp3

   [amelendez@node2Net1 ~]$ ping 10.3.2.101
   PING 10.3.2.101 (10.3.2.101) 56(84) bytes of data.
   64 bytes from 10.3.2.101: icmp_seq=1 ttl=62 time=3.64 ms

   [amelendez@node2Net1 ~]$ ping 10.3.2.102
   PING 10.3.2.102 (10.3.2.102) 56(84) bytes of data.
   64 bytes from 10.3.2.102: icmp_seq=1 ttl=62 time=4.65 ms

### Stage 3: Default routs

In order to have Internet access from all machines I need to setup  default routes on almost all machines. 

- For those machines inside LAN1 the default gateway will be router1.
- For machines inside LAN2 the default gateway will be router2.
- For router2 the gateway by defaul will be router1.

- Node1.net1.tp3

  *$ sudo ip route add default via 10.3.1.254 dev enp0s3*

   [root@node1Net1 amelendez]# ip r s
   default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
   10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102

- Node2.net1.tp3

  *$ sudo ip route add default via 10.3.1.254 dev enp0s3*

   [amelendez@nodeNet1 ~]$ ip r s
   default via 10.3.1.254 dev enp0s3 proto static metric 100
   10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.253 metric 100
   10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100

- Node1.net2.tp3

  *$ sudo ip route add default via 10.3.2.254 dev enp0s3*

   [amelendez@nodeNet2 ~]$ ip r s
   default via 10.3.2.254 dev enp0s3 proto static metric 100
   10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100
   10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.101 metric 100

- Node2.net2.tp3

  *$ sudo ip route add default via 10.3.2.254 dev enp0s3*

   [amelendez@dnsNet2 ~]$ ip r s
   default via 10.3.2.254 dev enp0s3 proto static metric 100
   10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100
   10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.102 metric 100

- router2

   *sudo ip route add default via 10.3.100.1 dev enp0s8*

   [amelendez@router2 ~]$ ip r s
   default via 10.3.100.1 dev enp0s8 proto static metric 101
   10.3.1.0/24 via 10.3.100.1 dev enp0s8 proto static metric 101
   10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.254 metric 102
   10.3.100.0/30 dev enp0s8 proto kernel scope link src 10.3.100.2 metric 101

#### Ping from node1.net2.tp3

   [root@node1Net1 amelendez]# ping 8.8.8.8
   PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
   64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=17.6 ms

#### Traceroute from node2.net1.tp3

   [amelendez@nodeNet1 ~]$ sudo traceroute -4 8.8.8.8
   traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
   1  _gateway (10.3.1.254)  2.214 ms  2.070 ms  2.584 ms
   2  192.168.164.2 (192.168.164.2)  3.405 ms  2.712 ms  3.009 ms


## Network services

Once every machine has internet, networks wil be differenciated:
- LAN1 will represent a client network (enterprise employee)
- LAN2 will represent a server room (where infrastructure servers are found)

Firstly, I will change some IP addresses of my machines inside the network to make my design more realistic:

Node1.net1.tp3 will rest the same.
Node2.net.tp3 will become dhcp.net1.tp3 and have 10.3.1.253/24 as static IP address.
Node1.net2.tp3 will become web.net2.tp3 and have 10.3.2.101/24 as static Ip address.
Node2.net2.tp3 will become dns.net2.tp3 and have 10.3.2.102/24 as static Ip address.

With this topology, no client will take part on LAN2.

This is simple by modifying these files:

 */etc/hostname/*

and

 */etc/sysconfig/network-scripts/ifcfg-enp0s3"*

### Stage 1: DHCP

Will give IP addresses to clients at will indicate how to join Internet. In order to be able to give out IP addresses it is on the client's network.

#### Setup

Firstly, I install my DHCP server:

 *$ dnf -y install dhcp-server*

Then I modified this file */etc/dhcp/dhcpd.conf* with the following configuration:

   #specify DNS server's hostname or IP address
   option domain-name-servers     1.1.1.1;

   #default lease time
   default-lease-time 600;

   #max lease time
   max-lease-time 7200;

   #this DHCP server to be declared valid
   authoritative;

   #specify network address and subnetmask

   subnet 10.3.1.0 netmask 255.255.255.0 {
      #specify the range of lease IP address
      range dynamic-bootp 10.3.1.50 10.3.1.99;
      #specify broadcast address
      option broadcast-address 10.3.1.255;
      #specify gateway
      option routers 10.3.1.254;
   }

I will then configure the file */etc/sysconfig/network-scripts/ifcfg-enp0s3"* and modify it so node1 will take addresses from the DHCP server:

   [amelendez@node1Net1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
   [sudo] password for amelendez:
   NAME=enp0s3
   DEVICE=enp0s3

   BOOTPROTO=dhcp
   ONBOOT=yes

   GATEWAY=10.3.1.254

#### Prove

- Asking for an IP address

 *sudo dhclient -r*
 *sudo dhclient*

- IP given

   [amelendez@node1Net1 ~]$ ip a
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
         valid_lft forever preferred_lft forever
   2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      link/ether 08:00:27:68:fb:43 brd ff:ff:ff:ff:ff:ff
      inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
         valid_lft 330sec preferred_lft 330sec
      inet6 fe80::a00:27ff:fe68:fb43/64 scope link
         valid_lft forever preferred_lft forever

- Routing table

   [amelendez@node1Net1 ~]$ ip r s
   default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 100
   10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 100
   10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100

- DNS address

It's been previously configured on my dhcpd.conf.

- Ping 

   [root@node1Net1 amelendez]# ping efrei.fr
   PING efrei.fr (51.255.68.208) 56(84) bytes of data.
   64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=1 ttl=127time=21.1 ms
   64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=2 ttl=127time=23.3 ms

### Stage 2: Serveur Web

This server will be hosting a basic webpage that simulates an enterprise workspace.

A web server allows downloading of files by help of HTTP protocol. If we download a standard file, written in HTML, with an adapted software, such as a web browser, which will print visually the HTML.

#### Installation

To install Nginx I used this command:

 *dnf install -y nginx*

#### Webpage

This part means the creation of a simple webpage:

I created the following directory: *var/www/efrei_site_nul/*
Which will contain all files of out webpage. It is commonly refered to as *webroot*.

After this, it is necessary to give access rights to nginx user. Without this step, the directory won't be accessible for the Nginx web server.

And create a file */var/www/efrei_site_nul/index.html* which will contain a random phrase, i wrote "Buenos días EFREI". Nginx must have access rights to this file too (-R).

   [amelendez@webNet2 ~]$ sudo chown nginx.nginx -R /var/www/efrei_site_nul/

   [amelendez@webNet2 ~]$ ls -l /var/www/efrei_site_nul/index.html
   -rw-r--r--. 1 nginx nginx 19 Oct  6 12:44 /var/www/efrei_site_nul/index.html

 #### Nginx configuration

 IN order to make sure Nginx will work as our webpage we must:

 - Indicate the webpage webroot
 - Indicate on what IP address and port the page will be accessible

 *sudo vi /etc/nginx/conf.d/web.net2.tp3.conf*

 server {
      # hostname
      server_name   web.net2.tp3;

      # IP address and port
      listen        10.3.2.101:80;

      location      / {
         # webroot
         root      /var/www/efrei_site_nul;

         # homepage
         index index.html;
   }
}

#### Firewall

HTTP trafic is encapsulated in TCP, and as I satated, we will open port 80:

   [amelendez@webNet2 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
   success

   [amelendez@webNet2 ~]$ sudo firewall-cmd --reload
   success

- Test

   [amelendez@webNet2 ~]$ sudo firewall-cmd --list-all
   public (active)
   target: default
   icmp-block-inversion: no
   interfaces: enp0s3 enp0s8
   sources:
   services: cockpit dhcpv6-client ssh
   ports: 80/tcp
   protocols:
   forward: yes
   masquerade: no
   forward-ports:
   source-ports:
   icmp-blocks:
   rich rules:


#### Nginx Test

To start the Nginx sevices:

 *$ sudo systemctl start nginx*

To make sure service will start when powering up the virtual machine:

[amelendez@webNet2 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service →/usr/lib/systemd/system/nginx.service.

To obtain service information:

 *$ sudo systemctl status nginx*

To get logs in case of problem:

*$ sudo journalctl -xe -u nginx*

- Local test

   [amelendez@webNet2 ~]$ curl http://10.3.2.101
   Buenos días EFREI

Curl is a linux command that lets HTTP requests. It will only print HTTM language.

- Testing from a client

   [amelendez@node1Net1 ~]$ curl http://10.3.2.101
   Buenos días EFREI

- With an address name

By modyfying our */etc/hosts* file from the client machine we will be able to print the page with it's hostname:

   [amelendez@node1Net1 ~]$ cat /etc/hosts
   127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
   ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
   #10.3.2.101     web.net2.tp3

   [amelendez@node1Net1 ~]$ curl web.net2.tp3
   Buenos días EFREI


## DNS Server

Host file can be compared as a personal file that storages telephone numbers. Each machine has one.

DNS Server will take the place of the host file by being a central, unique and public repertory.

### Stage 1: Installation

I will install the DNS tools on dns.net2.tp3 node.

 *$ sudo dnf update -y*

 *$ sudo dnf install -y bind bind-utils*

### Stage2: Configuration

In order to have a DNS running, three files must be configurated:

- **/etc/named.conf**

It is the pricnipal configuration file where general information is defined. For example, IP addresses or the port where the DNS server will be available. And the way to zone files must be defined too.

The changes I did:

   $ sudo cat /etc/named.conf
   options {
         listen-on port 53 { 127.0.0.1; any; };
         listen-on-v6 port 53 { ::1; };
         directory       "/var/named";
   [...] 
         allow-query     { localhost; any; };
         allow-query-cache { localhost; any; };

         recursion yes; 
   [...]

   #zone file reference
   zone "net2.tp3" IN {
      type master;
      file "net2.tp3.db";
      allow-update { none; };
      allow-query {any; };
   };
   #inverse zone reference
   zone "2.3.10.in-addr.arpa" IN {
      type master;
      file "net2.tp3.rev";
      allow-update { none; };
      allow-query { any; };
   };

Recursion allows to DNS Server to ask another DNS Server if he does not know the response. Once the response is obtained, it will answer the client stating that it is another server who granted the answer. This is called DNS resolver.

- **/var/name/net2.tp3.db**
This represents the zone file (IP -> nom):

   $TTL 86400
   @ IN SOA dns.net2.tp3. admin.net2.tp3. (
      2019061800 ;Serial
      3600 ;Refresh
      1800 ;Retry
      604800 ;Expire
      86400 ;Minimum TTL
   )

   ; Infos sur le serveur DNS lui même (NS = NameServer)
   @ IN NS dns.net2.tp3.

   ; Enregistrements DNS pour faire correspondre des noms à des IPs
   dns        IN A 10.3.2.102
   web        IN A 10.3.2.101

- **/var/named/net2.tp3.rev**
This represents the inverse zone file (IP -> nom):

   $TTL 86400
   @ IN SOA dns.net2.tp3. admin.net2.tp3. (
      2019061800 ;Serial
      3600 ;Refresh
      1800 ;Retry
      604800 ;Expire
      86400 ;Minimum TTL
   )

   ; Infos sur le serveur DNS lui même (NS = NameServer)
   @ IN NS dns.net2.tp3.

   ;Reverse lookup for Name Server
   102   IN PTR dns.net2.tp3.
   101   IN PTR web.net2.tp3.

Finally DNS service must be started:

 *$ sudo systemctl start named*

 *$ sudo systemctl enable named*

To obtain information about service:
$ sudo systemctl status named

To obtain logs in case of a proble:
$ sudo journalctl -xe -u named

### Stage 3: Firewall

DNS traffic cand be found encapsulated in UDP protocol:

   [amelendez@dnsNet2 ~]$ sudo firewall-cmd --add-port=53/udp --permanent
   success
   [amelendez@dnsNet2 ~]$
   [amelendez@dnsNet2 ~]$
   [amelendez@dnsNet2 ~]$ sudo firewall-cmd --reload
   success

The result:

   [amelendez@dnsNet2 ~]$ sudo firewall-cmd --list-all
   public (active)
   target: default
   icmp-block-inversion: no
   interfaces: enp0s3 enp0s8
   sources:
   services: cockpit dhcpv6-client ssh
   ports: 53/udp
   protocols:
   forward: yes
   masquerade: no
   forward-ports:
   source-ports:
   icmp-blocks:
   rich rules:

### Stage 4: Test

Dig command can do DNS requests from a Linux machine. It was obtained by downloading the *bind-utils* paquet while installing Rocky:

- Dig to find IP from web.net2.tp1:

[amelendez@node1Net1 ~]$ dig web.net2.tp3 @10.3.2.102

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3 @10.3.2.102
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 35101
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: a539519bbecd172301000000653a68a60a77e4ed4084de02 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 5 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Thu Oct 26 15:24:54 CEST 2023
;; MSG SIZE  rcvd: 85

- Use curl to visit web.net2.tp3 using its name:
   - Before doing it, I must delete previous modifications from my host file

      [amelendez@node1Net1 ~]$ curl web.net2.tp3
      Buenos días EFREI

### DHCP

- The DHCP configuration must be changed (dhcp.net1.tp3)

I changed DNS address to have dns.net2.tp3:

   #specify DNS server's hostname or IP address
   option domain-name-servers     10.3.2.102;

   #default lease time
   default-lease-time 600;

   #max lease time
   max-lease-time 7200;

   #this DHCP server to be declared valid
   authoritative;

   #specify network address and subnetmask

   subnet 10.3.1.0 netmask 255.255.255.0 {
      #specify the range of lease IP address
      range dynamic-bootp 10.3.1.50 10.3.1.99;
      #specify broadcast address
      option broadcast-address 10.3.1.255;
      #specify gateway
      option routers 10.3.1.254;
   }

- Dig:

  [amelendez@node1Net1 ~]$ dig web.net2.tp3

   ; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
   ;; global options: +cmd
   ;; Got answer:
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40359
   ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

   ;; OPT PSEUDOSECTION:
   ; EDNS: version: 0, flags:; udp: 1232
   ; COOKIE: 00a89ab5cc5d6c500100000065351fddd0f36b220151f37e (good)
   ;; QUESTION SECTION:
   ;web.net2.tp3.                  IN      A

   ;; ANSWER SECTION:
   web.net2.tp3.           86400   IN      A       10.3.2.101

   ;; Query time: 4 msec
   ;; SERVER: 10.3.2.102#53(10.3.2.102)
   ;; WHEN: Sun Oct 22 18:01:45 CEST 2023
   ;; MSG SIZE  rcvd: 85
