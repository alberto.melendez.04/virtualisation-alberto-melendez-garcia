# TP4: ARP Posioning

This projects searches to attack a network of two machines. The victim and the router. To do so, another machine will be added to de network, the attacker. I have chosen to use kali linux as attacking machine.


## Setup

For this last virtualization project, I will not be using GNS3, I will only connect the three machines by configuring a *Host-only Adapter* connection between them. On the configurations of VM Machine it is important to make sure that the *All Vm's* and *cable-connected* options are active.

It is also important to make sure everyone can ping eachother.

Once this has been done, to have a machine that works as a router I had to do the folowwing:
1. Add a NAT connection in the network settings of my node2
2. Configure node2 so that it will pass on those packets that are not directed to it:

    [amelendez@node2 ~]$ sudo sysctl -w net.ipv4.ip_forward=1
    net.ipv4.ip_forward = 1

    [amelendez@node2 ~]$ sudo firewall-cmd --add-masquerade
    success

    [amelendez@node2 ~]$ sudo firewall-cmd --add-masquerade --permanent
    success

3. Add a defaut gateway to node1 so that packets sent outside the network are directed to the router
   
 *$ sudo ip route add default via 192.168.95.21 dev enp0s3*

## First steps of the attack

I will use arping tool to spoof the identity of both machines, victim and router. By writing the attackant MAC address on both arp tables at the place of the machines, I will be able to recieve the communications that are done between them.

- The victim
IP address: 192.168.95.19
MAC address: 08:00:27:50:d4:71

- The attacker:
IP address: 192.168.95.20
MAC address: 08:00:27:c2:26:15 

- The router:
IP address: 192.168.95.21
MAC address: 08:00:27:10:f4:1a

The command to pass from kali is:

 *$ sudo arping -c 3 -s 08:00:27:c2:26:15 -S 192.168.95.21 -p 192.168.95.19*

- c To state the number of pings we want to be done.
- s To state the MAC address we want to use
- S To state the IP address we want to usurp
- p To state where we want to send the ping

With this command, I have sent three pings to my victim by making him believe that the router's direction is actually mine:

    [amelendez@node1 ~]$ ip n s
    192.168.95.21 dev enp0s3 lladdr 08:00:27:c2:26:15 STALE
    192.168.95.2 dev enp0s3 lladdr 08:00:27:da:da:fa STALE
    192.168.95.20 dev enp0s3 lladdr 08:00:27:c2:26:15 STALE
    192.168.95.1 dev enp0s3 lladdr 0a:00:27:00:00:0a DELAY

Doing the same to the router means that the next communications that happening between node1 and router will go through the attacking machine, who is free to modify, spy or do whatever he wants with these messages. For instance, when node1 tries to connect to a page such as facebook, the attacker could be able to redirect him to a phishing site and steal his information. This can only be done by spamming these type of messages.

[Here](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp4/wireshark_arpTest1.pcapng) you can see the different tests done with these tool.

## Going further

Here I will be using a python library called Scapy from my kali linux. This library allows us to do different networking attacks. For instance I only have used different functions to understand how it works, the objective is to be able to set up a script that will do a constant arp spoofing and that, when sent, will analyse dns requests form the victim to redirect to whatever we want.

Firstly, I tried basic tram crations. To do so I must open a python terminal with root rights. 
It is important to state that the first step is to import the scapy library: 
    *from scapy.all import*

For example:

This will create a ping echo request 
    ping = ICMP(type=8)

On the other hand, this will create a pong echo request (the reponse)
    ping2 = ICMP(type=0)

Another test I did was the folowing:
    send(IP(dst="192.168.95.21")/ICMP(id=1, seq=1, type=0))
By changing the type between 0 and 8 with the previous command, I remarked that a ping can be sent from my machine to another machine, in this case the router.

Furthermore, I can send a ping and demand a request from one machine that is not mine:
    send(IP(src="192.168.95.19", dst="192.168.95.21")/ICMP(id=1, seq=1, type=0))

Another interesting aspect is that we can also create ether frames by stating the MAC addresses.
