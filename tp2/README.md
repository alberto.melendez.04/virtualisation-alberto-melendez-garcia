# TP2

## Routage, DHCP et DNS

This TP's objective is to set up a basic router, a working DHCP server at a DNS server with internet access through a NAT connexion. We will be using virtual machines and teh GNS app in order to create a virtual LAN. 

Rocky Linux will be the OS used for this project.

### 1. Setup

I will be working with an SSH connection that facilitates the use of the virtual machine's terminal. 
For each virtual machine a hostname will be given. Here I show the four different names I have given my virtual machines:

 *$ sudo hostname <node1, router, dhcp, kali>*
 
As this is a temporary command, I changed the file containing the virtual machine's hostname:

 *$sudo vi /etc/hostname/*

And I wrote the hostname I chose. It is possible to consult the actual hostname of the machine by running this command:

 *$ hostname*

### 2. Routing

#### Stage 1: Addressing table

We will again clone the same virtual machine as in TP1.
During the first part of this project I will only be working with:

- router.tp2.efrei
- node1.tp2.efrei

Which will have the respective static IP addresses:

- 10.2.1.254/24
- 10.2.1.1/24

In order to do so I have manipulated on both machines the following file:

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

And in which I wrote this configuration:

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=(192.2.1.254/192.2.1.1)
    NETMASK=255.255.255.0

#### Stage 2: Reproduce the defined GNS3 topology

As I mentionned before, we will be working with node1.tp2.efrei and router.tp2.efrei, which are connected between each other through a switch.
These machines will also be connected to internet through a NAT connexion.

In order to do so, it is important to add a second network card to the router, which will be the one connected to de NAT.

#### Stage 3: Router.tp2.efrei configuration

I order to have internet access I will configure the router interface *enp0s9* si that it can get an automatic IP address via DHCP:

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s9*

   NAME=enp0s8
    DEVICE=enp0s8

    BOOTPROTO=dhcp
    ONBOOT=yes

To make sure I have a working internet connection I pinged google:

 *$ ping 8.8.8.8*

The ping was succesfull!

Here you can see my two IP addresses:

    [amelendez@router ~]$ ip a

    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWNgroup default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever

    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:f4:29:59 brd ff:ff:ff:ff:ff:ff
        inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
            valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:fef4:2959/64 scope link
        valid_lft forever preferred_lft forever

    4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:34:3d:49 brd ff:ff:ff:ff:ff:ff
        inet 192.168.164.132/24 brd 192.168.164.255 scope global dynamic noprefixroute enp0s9
            valid_lft 1220sec preferred_lft 1220sec
        inet6 fe80::c758:44d5:dad3:b147/64 scope link noprefixroute
 
In order to make my machine to work as a router, I will run the following commands:

    [amelendez@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1
    net.ipv4.ip_forward = 1

    [amelendez@router ~]$ sudo firewall-cmd --add-masquerade
    success

    [amelendez@router ~]$ sudo firewall-cmd --add-masquerade --permanent
    success

I am making sure that this machine will not throw the packages recieved that are not addressed to it.

#### Stage 4: node1.tp2.efrei configuration

At the moment, node1.tp2.efrei has been given a static IP address: 10.2.1.1

I will ping the router to see if node1.tp2.efrei and router.tp2.efrei are connected to the same LAN I will :
 
 *$ ping 10.2.1.254*

    [amelendez@node1 ~]$ ping 10.2.1.254
    PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
    64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=4.39 ms

In order to be able to access internet from node1.tp2.efrei I have added a permanent gateway:

 *$ sudo ip route add default
  via 10.2.1.254 dev enp0s3*

Then I added the same gateway in side the following file:

 *$ vi /etc/sysconfig/network*

    GATEWAY=10.2.0.254

To verify that the gateway has been correctly modified:

    [amelendez@node1 ~]$ ip r s
    10.2.1.0/24 via 10.2.1.254 dev enp0s3
    10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.1 metric 100
    192.168.95.0/24 dev enp0s8 proto kernel scope link src 192.168.95.5metric 101

In order to test my internet connection from node1.tp2.efrei I have pinged google:

    [amelendez@node1 ~]$ ping www.google.com
    PING www.google.com (142.250.201.164) 56(84) bytes of data.
    64 bytes from par21s23-in-f4.1e100.net (142.250.201.164): icmp_seq=1 ttl=127 time=15.8 ms

    --- www.google.com ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms
    rtt min/avg/max/mdev = 13.250/17.359/24.022/4.021 ms

To prove that my packets pass by router.tp2.efrei before going out on the internet I had to install traceroute and then do the following command:

    [root@node1 amelendez]# traceroute www.efrei.fr
    traceroute to www.efrei.fr (51.255.68.208), 30 hops max, 60 byte packets
    1  _gateway (10.2.1.254)  1.252 ms  1.093 ms  1.066 ms

### 3. DHCP Server

#### Stage 1: Installation and configuration of the DHCP server

Firstly, I modified the folowing file in order to give my DHCP server a fix address inside my LAN:

 *$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.2.1.253
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

In order to be able to access internet from dhcp.tp2.efrei I have added a permanent gateway:

 *$ sudo ip route add default
  via 10.2.1.254 dev enp0s3*

Then I added the same gateway in side the following file:

 *$ vi /etc/sysconfig/network*

    GATEWAY=10.2.0.254

Then I proceed to install and configure the DHCP server:

 *$ sudo dnf -y install dhcp-server*

 *$ vi /etc/dhcp/dhcpd.conf*

    option domain-name     "tp2.efrei";

    option domain-name-servers     1.1.1.1;

    default-lease-time 600;

    max-lease-time 7200;

    authoritative;

    subnet 10.2.1.0 netmask 255.255.255.0 {

        range dynamic-bootp 10.2.1.1 10.2.1.5;

        option broadcast-address 10.2.0.255;

        option routers 10.2.1.254;
    }

 *$ systemctl enable --now dhcpd*

 *$ firewall-cmd --add-service=dhcp*
 
 *$ firewall-cmd --runtime-to-permanent*

 *$ systemctl reboot dhcpd*

Now I have a running DHCP with a DNS address of a functionning CloudFare server (bonus). That only can give 5 IP addresses for those who connect the LAN. And that uses a specefic broadcast and gateway address.

#### Stage 2: DHCP testing

I will now delete all configuration done on node1.tp2.efrei so that my machine can take an IP address from the DHCP.

 *$ sudo nmcli con del enp0s3*

Verifications:

- IP obtenu

    [root@node1 amelendez]# ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWNgroup default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:33:58:15 brd ff:ff:ff:ff:ff:ff
        inet 10.2.1.1/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
        valid_lft 589sec preferred_lft 589sec
        inet6 fe80::a00:27ff:fe33:5815/64 scope link noprefixroute
        valid_lft forever preferred_lft forever
    
- Table de routage

    [root@node1 amelendez]# traceroute -4 8.8.8.8
    traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
    1  _gateway (10.2.1.254)  1.729 ms  1.682 ms  1.660 ms

- Internet access

    [root@node1 amelendez]# ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=11.9 ms

#### Stage 3: Wireshark

[Here](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp2/dhcp_wireshark.pcapng) you can find the DORA exchange between my node1.tp2.efrei and the dhcp.tp2.efrei server.

En regardant l'echange on peux voir la suivante information:

- DHCP Discovery

The request to know if there is an active DHCP server comes in broadcast and will get to everyone of the LAN members

- DHCP Offer

Here it is visible how the DHCP server is already addressing the virtual machine by a speciffic IP address, this is still not a final address because the machine has to accept the IP address.

- DHCP Request

This phase of the DORA exchange consist on the approval of the IP that the DHCP has given.

- DHCP ACK

Finally, the DHCP server will end the exchange by a message that acknowledges the given IP address.

### 4. ARP

#### Stage 1: ARP tables

Thanks to the ARP protocole we are able to obtain a MAC address when we know the machines's IP address.

In order to see which MAC addresses the router.tp2.efrei knows we run the next command:

 *$ ip n s*

    192.168.164.2 dev enp0s9 lladdr 00:50:56:f4:07:40 STALE
    10.2.1.253 dev enp0s3 lladdr 08:00:27:87:48:ec REACHABLE
    10.2.1.1 dev enp0s3 lladdr 08:00:27:33:58:15 REACHABLE

The firs IP address corresponds to that of the NAT connexion we added at the beggining of the proyect.

[Here](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp2/wireshark_arp.pcapng) you can see an ARP exchange between node1.tp2.efrei and router.tp2.efrei. Firstly, node1 will ask for the router MAC address and finally the router will answer with it's MAC address.

#### Stage 2: ARP Poisoning

In this last part of the project we have been asked to do an ARP poisoning. This means we will send a ping to one of our virtual machines but with a manipulated MAC address so it does not know the real ping source.

The objective is to usurp the identity of router.tp2.efrei by sending it a ping from another virtual machine.

In order to do this I have added a new virtual machine to the LAN, this time it will be a Kali the chosen OS.

My machine will automatically get an IP address from the DHCP server.

In order to be able to usurp the router's identity I wil run an arping command that looks like this:

 *$ arping -c 2 -s 08:00:27:39:9d:b2 -S 10.2.1.254 -p 10.2.1.1*

Before running this command I have looked on node1.tp2.efrei's ARP table in order to see the different MAC and IP addresses saved:

 *$ ip n s*

    [amelendez@node1 ~]$ ip n s
    10.2.1.254 dev enp0s3 lladdr 08:00:27:f4:29:59 STALE
    192.168.95.2 dev enp0s8 lladdr 08:00:27:86:64:d4 STALE
    192.168.1.254 dev enp0s3 lladdr 08:00:27:f4:29:59 STALE
    10.2.1.253 dev enp0s3 lladdr 08:00:27:87:48:ec STALE
    192.168.95.1 dev enp0s8 lladdr 0a:00:27:00:00:52 DELAY

As you can see, the connection between this machine and the machine that holds 10.2.1.254 (router) is stale. This means that if an exchange was to happen between both of them, a new ARP exchange will be held too.

Now I will try to force this exchange from my kali machine, by doing this, I will have successfully usurped the router's identity as node1 will not be able to recognise either kali's IP address but it will send the packages to my MAC address:

    ┌──(kali㉿kali)-[~]
    └─$ sudo arping -c 3 -s 08:00:27:39:9d:b2 -S 10.2.1.254 -p 10.2.1.1
    [sudo] password for kali:
    ARPING 10.2.1.1
    60 bytes from 08:00:27:33:58:15 (10.2.1.1): index=0 time=2.143 msec
    60 bytes from 08:00:27:33:58:15 (10.2.1.1): index=1 time=3.537 msec
    60 bytes from 08:00:27:33:58:15 (10.2.1.1): index=2 time=3.603 msec

    --- 10.2.1.1 statistics ---
    3 packets transmitted, 3 packets received,   0% unanswered (0 extra)
    rtt min/avg/max/std-dev = 2.143/3.094/3.603/0.673 ms

- c
    To state the number of pings we want to be done.

- s
    To state the MAC address we want to usurp

- S
    To state the IP address we want to usurp

- p
    To state where we want to send the ping

Now, if I see the ARP table of my node 1:

IThe following lines represent the updated ARP table of node1.tp2.efrei after the arping command:

    [amelendez@node1 ~]$ ip n s
    192.168.95.1 dev enp0s8 lladdr 08:00:27:39:9d:b2 DELAY
    10.2.1.254 dev enp0s3 lladdr 08:00:27:f4:29:59 REACHABLE
    10.2.1.253 dev enp0s3 lladdr 08:00:27:87:48:ec STALE
    192.168.95.2 dev enp0s8 lladdr 08:00:27:42:c4:b4 STALE

As you can see the connection is now reachable, which means that our usurpation was succesfull because we changed the previous MAC address. This means that during the time the connexion is reachable, there won't be an ARP exchange between the machines while exchanging packages. So it will directly exchange packages with my machine.

[Here](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp2/arp_poisoning.2.pcapng) you can see the ARP exchange between my kali and node1.tp2.efrei virtual machines.