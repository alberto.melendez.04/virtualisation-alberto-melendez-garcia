# TP1

## Remise dans le bain

**Virtualization** is the ability to run simultaneously several operating systems on a single computer.

This TP’s objective is to create a LAN with different virtual machines that will be connected to each other. There are several open source tools we will be working with:

1. Oracle VM: It’s a cross platform virtualization software that developers can use to run any software that could be run in a physical computer. 
2. GNS3: We will be using this software to emulate, configure and test our virtual connection between the virtual machines we are using. 
3. Wireshark: This software is a network packet analyzer. We will use it in order to read and capture the different interactions between our virtual machines.
4. Rocky Linux: It is the Linux distribution, created by Red Hat Enterprise Linux (RHEL), that we will be using. Even though  any Linux distribution could be used, Rocky Linux gives us some advantages, such as security or reliability.

Once we have installed all the mentioned software we start with the configuration of our virtual machines:
Our first virtual machine will define the basis of the rest of the machines we will be using as nodes within our connection. We will be cloning it. We configure it like this:
- Operating System: Red Hat 9.x (64-bit)
- Base Memory: 1024 MB
- Video Memory: 128 MB
- Adapter: NAT

Once the machine is up and running we will run different commands on it:

 *$sudo dnf update -y*

We update the software that installs, removes and packages on this linux distribution.

 *$sudo dnf install python bind-utils nmap nc tcpdump nano vim*

We install some tools that are necessary for the project.

 *$sudo setenforce 0*
 *$sudo nano /etc/selinux/config*

We change the state from enforcing to permissive.

    SELINUX=enforcing replace SELINUX=permissive

The enforcing state is the default state. With the permissive state, denials are logged but not enforced.

Once this has been done, we clone this virtual machine two times and we can start using GNS3. We will call the machines:
- node1.tp1.efrei
- node2.tp1.efrei

We will create a project in GNS3 and we add our machines so we can work with them:
1. Edit
2. Preferences
3. VirtualBox VMs
4. New
As we connect them with a cable and run them from GNS3, both will be powered on.
*In order to be able to connect the viertual machines between them we have to disable the nat connection so GNS3 will be able to work corrctly.*

### 1. Most simplest LAN
#### Stage 1: Define MAC address

 *$ ip addr show*

node1.tp1.efrei: 08:00:27:bb:4b:d9
node2.tp1.efrei: 08:00:27:eb:4c:96

#### Stage 2: Define a static IP on both machines
First, it is important to know the name of the interface that we want to change.
In this case: *enp0s3*

Secondly, I had to modify the file that corresponds to the interface:

 *$ sudo cd /etc/sysconfig/network-scripts*

As the file ifcfg-enp0s3 doesn’t exist i created it:

 *$ vi ifcfg-enp0s3*

Then, I have written the next lines inside it in order to give my virtual machines different static IP addresses:

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=(for node1: 10.1.1.1, for node2: 10.1.1.2)
    NETMASK=255.255.255.0

Finally, I reboot the connection:

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

To see if I was successful I will do again:

 *$ ip a*

#### Stage 3: Do a ping from one machine to the other

If we want to do it from node1 to node 2:
 *$ ping 10.1.1.2*
If we want to do it from node2 to node1
 *$ ping 10.1.1.1*
#### Stage 4: Wireshark
To start the Wireshark capture of the ping we do *click right* over the cable ethernet, knowing that the ping command must have been already written.
Ping works by sending an Internet Control Message Protocol (ICMP). First, a ping signal is sent to a specified address, then when it is received, the target responds by sending a reply packet (pong). In this case I captured a ping sent from node2 to node1 (filter by arp) See [Wireshark1](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp1/Capture_wireshark.pcapng)

#### Bonus
##### Is an ARP exchange necessary for the ping to function?
Yes, in order to know if anyone is using the target IP, an ARP exchange is held:
	“WHO HAS 10.1.1.1? Tell 10.1.1.2”
	“10.1.1.1 is at 08:00:27:bb:4b:d9. Tell 10.1.1.2”
	“WHO HAS 10.1.1.2? Tell 10.1.1.1”
	“10.1.1.2 is at 08:00:27:eb:4c:96. Tell 10.1.1.1”
As the ping also sends a response, there are two ARP exchanges held: First,  in order for the sender of the ping to know the target’s MAC destination. And the second one, in order for the target’s response to the ping to know the sender MAC address.

##### Use a command from node1.tp1.efrei to show the MAC address of node2.tp1.efrei:

 *$ ip n s*

As the ARP command has become useless we use this command instead. Using this command we are able to see the ARP Table of a device, which exists in every network connected device. We will only be able to see the MAC address of a device who has previously held an exchange with the device you are looking at the table from. This means that if we don’t do a ping, we won’t be able to see the MAC address of the node2.tp1.efrei.
This MAC address will remain enabled within a specific time lapse, when it ends, the MAC address will become stale and lastly deleted. While the address is enabled, an ARP exchange won’t happen between both devices because they know each other addresses already.

### 2. We add a switch

We use the **switch** to connect more than two machines between them.

#### Stage 1: Determine the MAC address of the three machines:

 *$ ip a*
node1.tp1.efrei: 08:00:27:bb:4b:d9
node2.tp1.efrei: 08:00:27:eb:4c:96
node3.tp1.efrei: 08:00:27:fe:05:99

#### Stage 2: Define a static IP on the three machines

    node1.tp1.efrei: 10.1.1.1/24
    node2.tp1.efrei: 10.1.1.2/24

In order to give node3.tp1.efrei we follow the same process we did previously:
We want to change the interface *enp0s3*

We go to this repertory
 *$ sudo cd /etc/sysconfig/network-scripts*

As the file ifcfg-enp0s3 doesn’t exist I created it:

 *$ vi ifcfg-enp0s3*

Then, I have written the next lines inside it in order to give my virtual machine a static IP address:

	NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.1.1.3
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

This will get us node3.tp1.efrei’s IP: 10.1.1.3/24

Once all the machines are connected I tested these pings:
    node 1 to node 2
    node 1 to node 3
    node 2 to node  3
As ping implies a response, there is no need to do the reverse testing.

### 3. DHCP Server

We will use the last virtual machine as a DHCP server, thanks to it, IP addresses will be given manually for those clients that ask one.

#### Stage 1: Give dhcp.tp1.efrei access to internet
In order to do so, we will have to add a NAT network card to the new virtual machine. Once it is running to test if it has internet access i have runned this command:

 *$ ping google.com*

To be able to connect to google it is necessary to have internet access. By doing a ping to google I can make sure, if it arrives correctly, that my virtual machine is connected to the Internet, which is the case.

#### Stage 2: Install and configure a DHCP server
We will be listing several commands used to configure a DHCP server on our  machine, these commands have been taken from this [article](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=dhcp&f=1):

1. We install de DHCP server on our machine:

 *$ dnf -y install dhcp-server*

2. I disabled the NAT adapter of my DHCP server so that GNS can work.

3. We will modify our DHCP configuration:

 *$ sudo vi /etc/dhcp/dhcpd.conf*

    
    default-lease-time 600;

    max-lease-time 7200;

    authoritative;

    subnet 10.1.1.0 netmask 255.255.255.0 {

        range dynamic-bootp 10.1.1.1 10.1.1.4;

        option broadcast-address 10.0.0.255;
    }

- Lease time: The duration of time that a DHCP server grants a device to use an IP address.
- Authoritative: Refers to a DHCp server explicitly configured to manage the DHCP process.
- Subnet and netmask: Refers to the network that we want the DHCP to work in.
- Range: The range stands for the different IP addresses that the DHCP will be able to give simoultaneously.
- Broadcast-address:  IP Address for which all devices on that network are enabled to receive messages.

Once this has been done, we will run the command that enables the dhcp server:

 *$ systemctl enable --now dhcpd*

4. As the firewall is running, we will have to allow the DHCP service:

 *$ firewall-cmd --ad-service=dhcpd*
 *$ firewall-cmd --runtime-to-permanent*

5. My DHCP is still not working because it doesn't have any static IP address inside the network, so I follow the same process as previously:

 *$ vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=static
    ONBOOT=yes

    IPADDR=10.1.1.253
    NETMASK=255.255.255.0

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*


To start my DHCP server  I run:

 *$ systemctl start dhcpd*

I can check it's status: 

 *$ systemctl status dhcpd*

    ● dhcpd.service - DHCPv4 Server Daemon
        Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
        Active: active (running) since Sat 2023-09-23 17:32:18 CEST; 3min 2s ago


#### Stage 3: Recover IP addresses automatically
By running my machines with GNS. I must change the nodes IP configuration because I had previously given them static IP addresses. I changed deleated this file:

 *$ vi /etc/sysconfig/network-scripts/ifcfg-enp0s3*

    NAME=enp0s3
    DEVICE=enp0s3

    BOOTPROTO=dhcp
    ONBOOT=yes

 *$ nmcli con reload*
 *$ nmcli con up enp0s3*

To make sure they have taken their IP addresses from the DHCP server I run from each terminal:

 *$ ip a*

There is another way. To check the DHCP client list:

 *$ cat /var/lib/dhcpd/dhcpd.leases*

    lease 10.1.1.1 {
    starts 6 2023/09/23 15:37:32;
    ends 6 2023/09/23 15:47:32;
    cltt 6 2023/09/23 15:37:32;
    binding state active;
    next binding state free;
    rewind binding state free;
    hardware ethernet 08:00:27:bb:4b:d9;
    uid "\001\010\000'\273K\331";
    }
    lease 10.1.1.2 {
    starts 6 2023/09/23 15:37:41;
    ends 6 2023/09/23 15:47:41;
    cltt 6 2023/09/23 15:37:41;
    binding state active;
    next binding state free;
    rewind binding state free;
    hardware ethernet 08:00:27:eb:4c:96;
    uid "\001\010\000'\353L\226";
    }
    lease 10.1.1.3 {
    starts 6 2023/09/23 15:38:00;
    ends 6 2023/09/23 15:48:00;
    cltt 6 2023/09/23 15:38:00;
    binding state active;
    next binding state free;
    rewind binding state free;
    hardware ethernet 08:00:27:ec:03:83;
    uid "\001\010\000'\354\003\203";
    }

#### Stage 4: Wireshark

**DORA**
- DHCP Demand:a DHCP client is requesting IP to the DHCP server, and this discover request is sent out in the form of a broadcast request using 255.255.255.255. This request will reach every device.
- DHCP Offer: Once the DHCP accepts the discover request sent by the client. The DHCP will offer some IP to the client. 
- DHCP Request: The client will select the IP address accordingly and request to the DHCP server that "I want to use this IP". So, the client will send a request to use the specific selected IP. 
- DHCP Acknowledge: The DHCP server will receive the request sent by the client machine and then acknowledge the requested IP. 

[Wireshark capture](https://gitlab.com/alberto.melendez.04/virtualisation-alberto-melendez-garcia/-/blob/main/tp1/wireshark_dhcp_exchange2.pcapng)

For those IP addresses who have already been attributed there is no need to exchange again a Discovery and Offer request, as you can see on the capture. But for those new, this exchange will happen (filter by dhcp).